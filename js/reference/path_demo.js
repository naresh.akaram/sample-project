const path = require('path');

//Base file name
console.log(`entire file src :-  ${__filename}`);
console.log(
  `dir src in which the file is saved :- ${path.dirname(__filename)}`
);
console.log(
  `dir src in which the file is saved :- ${path.basename(__filename)}`
);

//create path Object
let pathObj = path.parse(__filename);
console.log(`making object from path src :- ${path.parse(__filename)}`);
console.log(pathObj.root);
