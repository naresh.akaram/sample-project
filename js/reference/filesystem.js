const fspath = require('path');
const fs_fileModule = require('fs');
// create a new folder && mkdir is a async method
// fs_fileModule.mkdir(fspath.join(__dirname, '/test'), {}, function(err) {
//   if (err) throw err;
//   console.log('Folder created ...');
// });

//create and write to file

// fs_fileModule.writeFile(
//   fspath.join(__dirname, '/test', 'hello.txt'),
//   `hai this is my fist node js created file`,
//   function(err) {
//     if (err) throw err;
//     console.log(
//       'Congrats ... text file created and text is added to it- :)  Happy learning...'
//     );
//     fs_fileModule.appendFile(
//       fspath.join(__dirname, 'test', 'hello.txt'),
//       `text appended to file`,
//       function(err) {
//         if (err) throw err;
//         console.log('text appended');
//       }
//     );
//   }
// );

// Read file

// fs_fileModule.readFile(
//   fspath.join(__dirname, '/test', 'hello.txt'),
//   'utf8',
//   (err, data) => {
//     if (err) throw err;
//     console.log(data);
//   }
// );

//Rename file
fs_fileModule.rename(
  fspath.join(__dirname, '/test', 'hello.txt'),
  fspath.join(__dirname, '/test', 'hellorename.txt'),
  err => {
    if (err) throw err;
    console.log('file renamed');
  }
);
