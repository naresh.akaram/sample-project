const os = require('os');
console.log(os.platform());

// CPU Arch
console.log(os.arch());

//CPU Core info
console.log(os.cpus());

//Free memory
console.log(os.freemem());

// total memory
console.log(os.totalmem());

//Home dir

console.log(os.homedir());
