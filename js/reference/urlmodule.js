const url = require('url');
const myUrl = new URL(
  'http://mywebsite.com:8000/hello.html?id=100&status=active'
);

// Serialized URL

console.log(myUrl.href);
console.log(myUrl.toString());

// Host (root domain)  --  host return port no. if url host has port number

console.log(myUrl.host);

//Hostname - hostname doesnot return port no. if url host has port number

console.log(myUrl.hostname);

//Pathname
console.log(myUrl.pathname);

// Serialized query

console.log(myUrl.search);

//Params object
console.log(myUrl.searchParams);
