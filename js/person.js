// const person = {
//   name: 'naresh',
//   age: 30
// };
//Module Wrapper function
// (function(module, __dirname, __filename, exports, require) {});

class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  greeting() {
    console.log(`hai this is ${this.name} and my age is ${this.age}`);
  }
}
module.exports = Person;
